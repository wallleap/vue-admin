const { defineConfig } = require('@vue/cli-service')
const isProduction = process.env.NODE_ENV === 'production'
let externals = {}
let CDN = { css: [], js: [] }
if (isProduction) {
  externals = {
    echarts: 'echarts',
    vue: 'Vue',
    'vue-router': 'VueRouter',
    vuex: 'Vuex',
    axios: 'axios',
    dayjs: 'dayjs',
    'element-ui': 'ELEMENT',
    'vue-quill-editor': 'VueQuillEditor',
    'vuex-persistedstate': 'createPersistedState'
  }
  CDN = {
    css: [
      // element-ui css
      '//unpkg.com/element-ui@2.15.8/lib/theme-chalk/index.css',
      // quill css
      '//unpkg.com/quill@1.3.7/dist/quill.core.css',
      '//unpkg.com/quill@1.3.7/dist/quill.snow.css',
      '//unpkg.com/quill@1.3.7/dist/quill.bubble.css',
    ],
    js: [
      '//unpkg.com/echarts@5.3.2/dist/echarts.min.js',
      '//unpkg.com/vue@2.6.14/dist/vue.js',
      '//unpkg.com/vue-router@3.5.1/dist/vue-router.js',
      '//unpkg.com/vuex@3.6.2/dist/vuex.js',
      '//unpkg.com/axios@0.27.2/dist/axios.min.js',
      '//unpkg.com/dayjs@1.11.3/dayjs.min.js',
      '//unpkg.com/element-ui@2.15.8/lib/index.js',
      '//unpkg.com/quill@1.3.7/dist/quill.js',
      '//unpkg.com/vue-quill-editor@3.0.6/dist/vue-quill-editor.js',
      '//unpkg.com/vuex-persistedstate@3.2.1/dist/vuex-persistedstate.umd.js',
    ]
  }
}

module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: isProduction ? './' : '/',
  configureWebpack: {
    externals,
  },
  chainWebpack: config => {
    // CDN
    config.plugin('html').tap(args => {
      args[0].cdn = CDN
      return args
    })
  }
})
