import axios from 'axios'
import store from '@/store'
import router from '@/router'
import config from '@/config'
import { Message } from 'element-ui'

const baseURL = config.baseURL // API 基地址
export const CDNURL = config.CDNURL // CDN 加速域名

const myAxios = axios.create({
  baseURL,
})

// 定义请求拦截器
myAxios.interceptors.request.use(function (config) {
  const token = store.state.token
  if (token) {
    config.headers.Authorization = token
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

// 定义响应拦截器
myAxios.interceptors.response.use(function (response) {
  return response
}, function (error) {
  if (error.response.status === 401) {
    store.commit('updateToken', '')
    store.commit('updateUserInfo', {})
    router.push('/login')
    Message.error('登录状态无效，请重新登录')
  }
  return Promise.reject(error)
})

export default myAxios
