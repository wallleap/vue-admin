import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import config from './config'
import './elementUI/index.js'
import './vueEditor/index.js'
import dayjs from 'dayjs'
import './assets/global.scss'

Vue.config.productionTip = false
Vue.prototype.$config = config
Vue.prototype.$formatDate = (dateObj) => {
  return dayjs(dateObj).format('YYYY-MM-DD HH:mm:ss')
}

export const vm = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
