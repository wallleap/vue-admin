import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import { getUsersInfoAPI } from '@/api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: '',
    userInfo: {},
    isCollapse: false
  },
  getters: {
    nickname: state => state.userInfo.nickname,
    username: state => state.userInfo.username,
    avatar: state => state.userInfo.user_pic
  },
  mutations: {
    updateToken (state, newToken) {
      state.token = newToken
    },
    updateUserInfo (state, info) {
      state.userInfo = info
    },
    updateCollapse (state, val) {
      state.isCollapse = val
    },
  },
  actions: {
    // 定义初始化用户基本信息的 action 函数
    async getUserInfoActions (store) {
      const { data: res } = await getUsersInfoAPI()
      if (res.code === 0) {
        store.commit('updateUserInfo', res.data)
      }
    }
  },
  modules: {
  },
  plugins: [createPersistedState()]
})
