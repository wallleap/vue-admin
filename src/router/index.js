import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('@/views/layout'),
    redirect: '/home',
    children: [
      {
        path: 'home',
        name: 'home',
        meta: {
          title: '首页'
        },
        component: () => import('@/views/home')
      },
      {
        path: 'user-info',
        name: 'user-info',
        meta: {
          title: '个人中心 > 个人信息'
        },
        component: () => import('@/views/user/userInfo')
      },
      {
        path: 'user-avatar',
        name: 'user-avatar',
        meta: {
          title: '个人中心 > 修改头像'
        },
        component: () => import('@/views/user/userAvatar')
      },
      {
        path: 'user-pwd',
        name: 'user-pwd',
        meta: {
          title: '个人中心 > 修改密码'
        },
        component: () => import('@/views/user/userPwd')
      },
      {
        path: 'art-cate',
        name: 'art-cate',
        meta: {
          title: '文章管理 > 文章分类'
        },
        component: () => import('@/views/article/artCate')
      },
      {
        path: 'art-list',
        name: 'art-list',
        meta: {
          title: '文章管理 > 文章列表'
        },
        component: () => import('@/views/article/artList')
      },
    ]
  },
  {
    path: '/reg',
    component: () => import('@/views/register')
  },
  {
    path: '/login',
    component: () => import('@/views/login')
  }
]

const router = new VueRouter({
  routes
})

const whiteList = ['/login', '/reg']

// 全局前置路由守卫
router.beforeEach((to, from, next) => {
  const token = store.state.token
  if (token) {
    if (!store.state.userInfo.username) {
      store.dispatch('getUserInfoActions')
    }
    if (to.path === '/login') { // fix: 直接访问登录页，登录另一个帐号，显示信息不正确
      store.commit('updateUserInfo', {})
    }
    next()
  } else {
    if (whiteList.includes(to.path)) {
      return next()
    } else {
      next('/login')
    }
  }
})

export default router
